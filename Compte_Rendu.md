---
title: La capitale du péché - Compte rendu
author: Timothée ZERBIB
date: 29 avril 2019
---


# La capitale du péché  <!-- OMIT IN TOC -->

- [Architecture](#architecture)
  - [Routeur](#routeur)
  - [MVC](#mvc)
  - [Petit point sur les modèles](#petit-point-sur-les-mod%C3%A8les)
- [Implémentation](#impl%C3%A9mentation)
  - [Accueil](#accueil)
  - [Inscription](#inscription)
  - [Catégorie](#cat%C3%A9gorie)
  - [Notation](#notation)
  - [Liste des utilisateurs](#liste-des-utilisateurs)
- [Déploiement](#d%C3%A9ploiement)
  - [BDD](#bdd)
- [Conclusion](#conclusion)
- [TODO](#todo)


Ce rapport a été écrit initialement au format markdown. Si vous ne possédez pas 
d'éditeur capable de visualiser correctement ce format, vous pouvez le 
retrouver sur le gitlab à l'adresse :  
<https://gitlab.unistra.fr/tzerbib/la-capitale-du-peche>  
Ceci est la version PDF.


## Architecture  

### Routeur  

Chaque requête se fait vers la page [index.php](WEB/index.php) qui sert de routeur au site.  
Les requêtes ont pour forme :  

```
https://chemin/index.php/controller|view|param1|param2|...|paramN
```  

Lors du premier appel au routeur, celui-ci va créer une session dans 
laquelle il stocke l'adresse racine du site car les redirections
php (header('Location: ...') prennent en compte l'URL actuelle du client et
donc les paramètres controller, vue...). Pour ce faire, il va utiliser
l'URL actuelle (avec $_SERVER['REQUEST_URI']) et lui enlever tous les
paramètres séparés par des '|'.  

Le routeur traite ensuite la requête. Il retravaille l'URL afin que des 
requêtes comme https://chemin/index.php et https://chemin/index.php/
mènent au même endroit. Puis il lit les arguments controller et vue
(s'ils sont vides, le routeur les remplace automatiquement par des paramètres 
par défaut : controlleur = accueil, vue = view) construit un objet de type
controller (si un tel objet existe) et appelle sa méthode vue (si une telle
méthode existe pour l'objet).  

Dans le cas où le controlleur ou sa méthode n'a pas été trouvé, le routeur
sert au client une page d'erreur.  


### MVC  

L'intégralité du site respecte le concept MVC.  
Chaque page est servie par un **routeur** qui appelle une méthode d'un 
controlleur.  
Ce **contrôleur** est chargé de vérifier l'intégrité de la requête (si elle 
possède le bon nombre d'arguments, la méthode GET/POST est respectée, 
l'utilisateur possède les bons droits...). Il peut également accéder à la base
de données au travers d'un **modèle** (chaque table de la base possède un modèle
qui lui est propre) et envoie des informations au client au travers des
**vues**.  


### Petit point sur les modèles

Tous les modèles sont basés sur le même... modèle.  
Il comprennent un attribut par attribut de la table correspondante, ainsi que
certains attributs supplémentaires simplifiant leur manipulation
(la signification d'un droit par exemple).  
Chaque attribut possède sa propre fonction accesseur et modificateur.
Leur constructeur initialise certains champs en fonction du nombre d'arguments
reçus.  
Ils implémentent les fonctions d'accès à la BDD par appel à des procédures 
stockées.
Tous les remplacements de variables lors d'appel à des procédures sont effectués
à l'aide de la fonction bindValue afin d'éviter toute injection SQL par un 
utilisateur malveillant.


## Implémentation  

### Accueil

La plupart des pages accessibles sur le site sont construites autour d'une
structure commune. Celle-ci est décrite dans la méthode
[view](WEB/controllers/accueil.php#L4) du controlleur
[Controller_Accueil][Controller_Accueil].  

Au premier appel à la méthode view, un utilisateur est instancié. Par défaut,
il s'agit d'un Visiteur. Une session étant déjà commencée par le routeur, j'en
ai profité pour stocker l'utilisateur instancié dans une variable de session 
sous forme de chaine de caractères (ainsi transformé, un objet ne prend que
peu de place en mémoire permettant au serveur de supporter de nombreux 
visiteurs simultanés). C'est dans cet objet utilisateur que sont contenues 
toutes les données nécessaires à l'affichage (le pseudo pour la barre de 
navigation), mais aussi des données complémentaires comme son rôle (la 
signification du rôle est également sauvegardée afin d'éviter un trop grand 
nombre d'appels à la BDD).  

L'authentification telle que décrite dans ce contrôleur se fait par appel à
la méthode [exists](WEB/models/users.php#L154) du modèle [users][User].
Selon le retour de cette fonction, un message d'erreur est affiché ou 
l'instance d'utilisateur est modifiée pour correspondre à l'utilisateur
connecté.  

C'est aussi dans ce contrôleur qu'est géré l'appel à certaines sous-pages
(la page de [contexte](WEB/views/corps/viewCorpsAPropos.php) et celle de 
[contact](WEB/views/corps/viewCorpsContact.php)).  


### Inscription

L'inscription est gérée par le contrôleur
[Controller_Register][Controller_Register]

La page d'inscription est une des rares pages à ne pas appeler la vue de 
l'accueil (J'avais pour l'inscription envie d'un style plus épuré).  

L'inscription d'un utilisateur se fait par la réception d'un formulaire.  
Après vérification de l'intégrité des données (existence + transformation avec 
htmlentities() + conformité de l'adresse email et égalité des deux champs de 
mot de passe, sous peine de quoi un message d'erreur est renvoyé dans un div
spécialement prévu à cet effet), un nouvel utilisateur est créé en BDD par 
la méthode [create](WEB/models/users.php#L189) du modèle [users][User].  
C'est cette méthode qui se charge de vérifier l'unicité des différents champs
(login, email...).


### Catégorie  

Toute la gestion des catégories, que ce soit l'obtention de la liste des 
catégories ou la visualisation d'un topic bien précis, est décrite dans le
contrôleur [Controller_Categories][Controller_Categories].  

Ma liste des catégories étant affichée comme une sous-page de la page 
accueil, il a été nécessaire de sauvegarder la liste des catégories dans une
variable de session afin de permettre l'accès à ces données par la vue (chargée 
dans le contrôleur accueil). Cette liste est donc transformée en chaine de 
caractères pour sa sauvegarde et supprimée après l'affichage de la sous-page.  

L'accès aux topics se fait soit par le menu de navigation à gauche (flèche à
droite de "Catégories" -> "Catégorie X" -> "Topic Y") soit par la page qui 
liste les topics par catégories (menu de navigation rubrique "Catégories", 
appuyer directement sur le bouton et non sur la flèche).

La liste des catégories est obtenue par appels successifs aux fonctions
[viewAll](WEB/controllers/categories#L4)
du contrôleur [Categories][Controller_Categories],
[view](WEB/controllers/accueil#L4) du contrôleur [Accueil][Controller_Accueil] 
et [liste_categories](WEB/controllers/categories#L191) du contrôleur 
[Categories][Controller_Categories].  
Les catégories sont listées par date de création et les topics par dernière 
modification (ajout d'un commentaire ou note du topic). Cet ordonnancement est
défini dans les procédures
[liste_categories](BDD/PROCEDURES/liste_categories.sql) et
[get_topics_categorie](BDD/PROCEDURES/get_topics_categorie.sql),
procédures appelées par la méthode statique
[getCategories](WEB/models/categorie.php#L99) du modèle [Catégorie][Categorie]


La méthode [view](WEB/controllers/categorie.php#L17) permet l'affichage d'un 
topic (les topics sont dans ce site considérés non comme une simple suite de
commentaires, mais comme un texte principal = sujet, suivi d'une série de 
réactions = commentaires).  
Il était prévu au départ de n'afficher qu'un nombre restreint de commentaires
sur un topic, et un système de pagination aurait permis de changer la liste
des commentaires affichés, mais le manque de temps m'a empêché d'implémenter
cela (il en reste cependant les prémices puisque lors de la vérification du 
nombre de paramètres passés à la méthode, si le paramètre page n'est pas donné,
il est initialisé à la page par défaut).  
Dans cette fonction, différentes vérifications s'effectuent (méthode de la 
requête, nombre d'arguments) puis une instance de topic est créée par le
modèle [Topic][Topic] et est remplie si le topic existe par la méthode 
[exists](WEB/models/topic.php#L151) de ce même modèle (dans le cas contraire, 
la page d'erreur 404 est affichée).  
Enfin, comme souvent l'affichage est laissé au contrôleur 
[accueil][Controller_Accueil] en spécifiant la
[sous-page de topic](WEB/views/corps/viewCorpsTopic.php).  


La page d'affichage d'un topic a été pour moi la plus intéressante car pour
l'envoi des informations de l'utilisateur (commentaire, notation...)
j'ai choisi d'utiliser l'envoi de données asynchrones avec AJAX.
Ainsi, il est possible de commenter un topic, de le noter et de 
plussoyer/moinssoyer un commentaire sans recharger la page.
Vous pourrez alors remarquer qu'au moment où vous postez un commentaire, le 
bouton sur lequel vous avez cliqué change d'aspect le temps de recevoir la 
confirmation de réception du serveur (pour une meilleure visualisation, je sais 
que certains navigateurs permettent de simuler du ping). De même, un spinner
est apparent lorsque vous notez un topic jusqu'à réception d'un retour du 
serveur.  

Le traitement effectué par le serveur lors du post d'un nouveau commentaire 
est décrit dans la méthode [commenter](WEB/controllers/categories.php#L103)
du contrôleur [categories][Controller_Categories].  
Cette méthode vérifie le type d'envoi, la réception des données attendues, 
le nombre d'arguments attendu pour la méthode, les droits de l'utilisateur 
(ici le fait d'être connecté), transforme les données envoyées avec la fonction
htmlentities et retourne un tableau [réussite, commentaire] au format JSON
(Il fallait pour le commentaire retenir à la fois son contenu, mais aussi
son créateur et son id. À noter que pour la transformation d'un commentaire
en JSON, il a fallu que le modèle [Commentaire][Commentaire] implémente l'interface
JsonSerializable pour redéfinir la fonction
[jsonSerialize](WEB/models/commentaire.php#L120)).

Du côté client, la requête AJAX est décrite par la variable
[HttpClient](WEB/public/js/topic.js#L1) et l'envoi de commentaires par une
[vérification](WEB/public/js/topic.js#L29) sur l'envoi du formulaire.  
La fonction de [callback](WEB/public/js/topic.js#L46) quant à elle remplit un
div particulier si une erreur est survenue ou construit tout une partie du DOM
pour ajouter le commentaire si la réponse du serveur est positive.  


### Notation 

Lorsque vous êtes sur la page d'un topic, il vous est possible de le noter.  
Là encore, il s'agit d'un envoi asynchrone avec AJAX.
Plusieurs fonctions côté client permettent le changement de l'affichage de
la note d'un topic (possibilité de noter un topic en passant sa souris
sur les étoiles en haut, affichage de la moyenne du topic sinon).
Ces fonctions sont décrites dans le fichier [topic.js](WEB/public/js/topic.js).

Du côté serveur, c'est le contrôleur [Notation][Controller_Notation] qui gère
à la fois la [notation d'un topic](WEB/controllers/notation.php#L95)
et celle d'un [commentaire](WEB/controllers/notation.php#L4).  
Ces deux fonctions ont un fonctionnement similaire. Elles vérifient le type
de la requête, la réception des données attendues, le nombre d'arguments de la
requête, les droits de l'utilisateur (statut connecté), transforment la note 
reçue avec htmlentities (quand bien même cette note est envoyée par javascript 
sans passer par un formulaire, un utilisateur malveillant pourrait modifier le 
comportement de ce script sur son navigateur) puis en entier avant d'appeler
le modèle [Topic_Note][Topic_Note] (respectivement 
[Commentaire_Note][Commentaire_Note]) qui va transformer la note pour qu'elle 
appartienne à l'ensemble [0-10] (respectivement {-1, 1}) et de 
renvoyer un tableau [réussite, moyenne]).  

Pour la notation d'un topic (respectivement d'un commentaire), c'est la 
procédure [noter_topic](BDD/PROCEDURES/noter_topic.sql) (respectivement
[noter_commentaire](BDD/PROCEDURES/noter_commentaire.sql)) qui se charge en plus
de l'insertion de mettre à jour la moyenne et la date de dernière
modification du topic (respectivement du commentaire) et qui renvoie la moyenne
nouvellement calculée.


### Liste des utilisateurs

Lorsque l'on est connecté en tant qu'administrateur, notre menu de navigation
est doté d'un lien supplémentaire, la liste des membres.  

Cette liste est donnée par la méthode
[viewAll](WEB/controllers/utilisateurs.php#L4) du contrôler [Utilisateurs][Controller_Utilisateurs].  
Cette méthode se charge de la vérification du type de la requête, et des droits
(car quand bien même ce lien n'est accessible que sur le menu de 
l'administrateur, il reste possible de taper le bon URL). La vérification des 
droits est effectuée par un appel à la méthode
[hasPermissions](WEB/models/users.php#L274) du modèle [User][User].  
Une fois ces vérifications effectuées, la liste des utilisateurs est donnée par
la méthode statique [getUsers](WEB/models/users.php#L285) du mème modèle.  
Enfin, un appel à la méthode [view](WEB/controllers/accueil.php#L4) du 
contrôleur [Controller_Accueil][Controller_Accueil] après redéfinition
de la sous-page permet l'affichage de la susdite liste.


Sur cette liste, il est possible de supprimer chaque utilisateur 
individuellement (une confirmation est tout de même demandée avant d'effectuer la suppression).  
La suppression d'un utilisateur est effectuée par la méthode
[delete](WEB/controllers/utilisateurs.php#L44) du contrôleur
[Utilisateurs][Controller_Utilisateurs].  
Comme toujours, cette méthode vérifie le type de la requête, les droits, le
nombre de paramètres, transforme les paramètres et appelle la méthode
[delete](WEB/models/users.php#L263) du modèle [User][User].


## Déploiement  

### BDD  

Chaque table de la base de données peut être créée puis initialisée à l'aide des
fichiers du dossier BDD/TABLES et BDD/INITS. L'ordre à respecter est décrit dans
le fichier d'[initialisation](BDD/Initialisation.md). Certaines initialisations
ne sont pas nécessaires mais permettent de démarrer avec un site moins vide...  

Les procédures peuvent être créées depuis les fichiers du dossier BDD/
PROCEDURES.  
Toutes les requêtes à la base de données effectuées dans les modèles sont des
appels à des procédures stockées.  

Les paramètres d'accès à la BDD sont décrits dans le fichier
[bdd.php](WEB/models/bdd.php)


## Conclusion

Par manque de temps, je n'ai pas pu implémenter toutes les fonctionnalités 
voulues. J'ai donc choisi les plus disparates afin de montrer un peu de
chacune.  

Ce forum contient donc une partie frontend avec un aspect
graphique travaillé et du javascript (gestion des notations, requêtes AJAX).

Mais j'ai principalement travaillé la partie backend avec la mise en place d'un
routeur (et je me suis compliqué la tâche avec une version faite main de la 
transformation du query string).  
Mon architecture est entièrement MVC, chaque requête est envoyée au routeur qui 
appelle un contrôleur qui se charge de vérifier/préparer les données, appeler 
un modèle pour les accès à la base de données et appeler les vues pour 
l'affichage des données.  
Les contrôleurs permettent aussi une gestion efficace des droits et vérifient
systématiquement les données reçues.
Pour la partie BDD, j'ai décris chacune de mes tables et procédures dans des 
fichiers spécifiques.
Chaque requête faite par les modèles à la BDD se font par des procédures 
stockées et se préviennent d'éventuelles injections SQL.

## TODO

Mais comme je l'ai dit, je n'ai pas pu réaliser tout ce que je comptais faire.

La structuration de mon site était prévue pour que les changements de sous-pages
se fasse par requêtes AJAX, évitant de recharger les contenus identiques entre 
ces différentes pages (la barre de navigation par exemple).

J'avais prévu d'ajouter une page d'accès/modification/suppression de son profil.
(le lien est déjà présent dans le menu de navigation mais ne mène nulle part)

Il était également prévu de pouvoir supprimer commentaires/topics pour
le modérateur (qui bien que présent dans la table, n'a encore aucun rôle 
particulier ces fonctionnalités n'étant pas présentes).

Enfin, je souhaitait afficher un nombre limité de commmentaires des topics 
par pages (pour çela, il m'était possible soit de n'afficher dans la vue que
les nbTopicsParPage*numPage topics soit directement dans la requête à la BDD 
avec les options LIMIT (nombre de résultat) et OFFSET (numéro du premier 
résultat). La première implémentation plus lourde en stoquage mémoire limite le 
nombre d'accès à la BDD ainsi les deux choix sont justifiables).




[Controller_Accueil]: WEB/controllers/accueil.php
[Controller_Register]: WEB/controllers/register.php  
[Controller_Categories]: WEB/controllers/categories.php
[Controller_Notation]: WEB/controllers/notation.php
[Controller_Utilisateurs]: WEB/controllers/utilisateurs.php

[User]: WEB/models/users.php
[Categorie]: WEB/models/categorie.php
[Topic]: WEB/models/topic.php
[Topic_Note]: WEB/models/topic_note.php
[Commentaire]: WEB/models/commentaire.php
[Commentaire_Note]: WEB/models/commentaire_note.php