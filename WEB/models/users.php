<?php

require_once "model_base.php";

class User extends Model_Base
{
  protected $_id;
  protected $_login;
  protected $_password;
  protected $_pseudo;
  protected $_email;
  protected $_droit;
  protected $_hierarchie;
  protected $_signification;
  protected $_connecte;


  public function __construct()
  {
    $ctp = func_num_args(); // nb arguments passés
    $args= func_get_args(); // tableau avec liste arguments
    
    switch($ctp)
    {
      case 4: // login, password, pseudo, email
      {
        $this->set_email($args[3]);
      }
      case 3: // login, password, pseudo
      {
        $this->set_pseudo($args[2]);
      }
      case 2: // login, password
      {
        $this->set_password($args[1]);
      }
      case 1: // Just the login
      {
        $this->set_login($args[0]);
      }
      case 0:
      {
        $this->set_droit("Visiteur");
        $this->set_connecte(false);
        break;
      }
      default:
      {
        throw new Exception("Nombre d'arguments du contructeur incorrect.");
      }
    }
  }


  public function set_id($id)
  {
    $this->_id = $id;
  }

  public function get_id()
  {
    return $this->_id;
  }


  public function set_login($l)
  {
    $this->_login = $l;
  }

  public function get_login()
  {
    return $this->_login;
  }


  public function set_password($p)
  {
    $this->_password = $p;
  }

  private function get_password()
  {
    return $this->_password;
  }


  public function set_pseudo($p)
  {
    $this->_pseudo = $p;
  }

  public function get_pseudo()
  {
    return $this->_pseudo;
  }


  public function set_email($m)
  {
    $this->_email = $m;
  }

  public function get_email()
  {
    return $this->_email;
  }


  public function set_droit($d)
  {
    $this->_droit = $d;
  }

  public function get_droit()
  {
    return $this->_droit;
  }


  public function set_hierarchie($h)
  {
    $this->_hierarchie = $h;
  }

  public function get_hierarchie()
  {
    return $this->_hierarchie;
  }


  public function set_signification($s)
  {
    $this->_signification = $s;
  }

  public function get_signification()
  {
    return $this->_signification;
  }


  public function set_connecte($c)
  {
    $this->_connecte = $c;
  }

  public function get_connecte()
  {
    return $this->_connecte;
  }


  public function exists()
  {
    $statement = self::$_db->prepare('CALL login_exists(:l)');
    $statement->bindValue(':l', $this->get_login(), PDO::PARAM_STR);
    $statement->execute();

    if($result = $statement->fetch(PDO::FETCH_ASSOC))
    {
      $statement->closeCursor();
      if(password_verify($this->get_password(), $result['password']))
      {
        $this->set_id($result['id']);
        $this->set_password($result['password']);
        $this->set_pseudo($result["pseudo"]);
        $this->set_email($result['email']);
        $this->set_droit($result['droit']);

        // Enregistrement du bon droit
        $req = self::$_db->query("CALL info_droit_id(".$result['droit'].")");
        $result = $req->fetch(PDO::FETCH_ASSOC);
        $this->set_hierarchie($result['hierarchie']);
        $this->set_signification($result['signification']);
        $req->closeCursor();

        $this->set_connecte(true);
        return 0; // OK
      }
      else
      {
        return 1; // Mot de passe invalide
      }
    }
    return 2; // Login invalide
  }

  public function create()
  {
    // Vérification de l'unicité du login
    $statement = self::$_db->prepare('CALL login_exists(:l)');
    $statement->bindValue(':l', $this->get_login(), PDO::PARAM_STR);
    $statement->execute();

    if($result = $statement->fetch(PDO::FETCH_ASSOC))
    {
      return 2; // Login déjà pris
    }
    $statement->closeCursor();
    
    // Vérification de l'unicité du pseudo
    $statement = self::$_db->prepare('CALL pseudo_exists(:p)');
    $statement->bindValue(':p', $this->get_pseudo(), PDO::PARAM_STR);
    $statement->execute();

    if($result = $statement->fetch(PDO::FETCH_ASSOC))
    {
      return 3; // Pseudo déjà pris
    }
    $statement->closeCursor();

    // Vérification de l'unicité de l'email
    $statement = self::$_db->prepare('CALL email_exists(:e)');
    $statement->bindValue(':e', $this->get_email(), PDO::PARAM_STR);
    $statement->execute();

    if($result = $statement->fetch(PDO::FETCH_ASSOC))
    {
      return 4; // Email déjà pris
    }
        
    else
    {
      $statement->closeCursor();

      // Récupération de l'id du droit "Membre" (= par défaut)
      $result = self::$_db->query('CALL info_droit("Membre")');
      $id = $result->fetch(PDO::FETCH_ASSOC)['id'];
      $result->closeCursor();
      
      // Ajout dans la table
      $statement = self::$_db->prepare('CALL utilisateur_create(:l, :psw, :psd,
                                                                :e, :d)');
      $statement->bindValue(':l', $this->get_login(), PDO::PARAM_STR);
      $statement->bindValue(':psw',
                            password_hash($this->get_password(),
                                          PASSWORD_DEFAULT),
                            PDO::PARAM_STR);
      $statement->bindValue(':psd', $this->get_pseudo(), PDO::PARAM_STR);
      $statement->bindValue(':e', $this->get_email(), PDO::PARAM_STR);
      $statement->bindValue(':d', $id, PDO::PARAM_INT);
      if(!$statement->execute())
      {
        return 1; // Erreur lors de l'insertion dans la table
      }
      return 0; // OK
    }
  }

  public function changePassword($newPassword)
  {
    $statement = self::$_db->prepare('UPDATE Utilisateur SET password = :p WHERE login = :l');
    $statement->bindValue(':l', $this->get_login(), PDO::PARAM_STR);
    $statement->bindValue(':p', password_hash($newPassword, PASSWORD_DEFAULT), PDO::PARAM_STR);
    if(!$statement->execute())
    {
      throw new Exception("Erreur lors de la modification du mot de passe dans la BDD.");
    }
    $this->set_password($newPassword);
  }

  public function delete()
  {
    $statement = self::$_db->prepare('CALL delete_user(:i)');
    $statement->bindValue(':i', $this->get_id(), PDO::PARAM_INT);
    if(!$statement->execute())
    {
      return 1;
    }
    return 0;
  }

  public function hasPermissions($role)
  {
    $statement = self::$_db->prepare('CALL info_droit(:r)');
    $statement->bindValue(':r', $role, PDO::PARAM_STR);
    $statement->execute();
    $result = $statement->fetch(PDO::FETCH_ASSOC);
    
    return ($this->get_hierarchie() >= $result['hierarchie']);
  }


  public static function getUsers()
  {
    self::$_db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    $statement = self::$_db->query('CALL liste_users()');

    $table = array();
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();

    foreach($result as $tabVal)
    {
      $u = new User($tabVal['login']);
      $u->set_id($tabVal['id']);
      $u->set_pseudo($tabVal['pseudo']);

      $statement = self::$_db->prepare('CALL info_droit_id(:i)');
      $statement->bindValue(':i', $tabVal['droit'], PDO::PARAM_INT);
      $statement->execute();
      $droit = $statement->fetch(PDO::FETCH_ASSOC);
      $u->set_signification($droit['signification']);
      $statement->closeCursor();

      $table[] = serialize($u);
    }
    return $table;
  }
}