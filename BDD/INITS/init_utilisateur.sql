/*
 * Fichier d'initialisation de la table Utilisateur
*/


-- Insertion d'un membre fantome (pour les suppressions)
INSERT INTO Utilisateur(id, login, password, pseudo, email, droit)
  VALUES
  (
    1,
    "ghost",
    "$2y$10$rgzp3YNtRDBCMYyaNb7SYOK7e96AaaLqaPlRUKdHhOij.TP0J1.ZG",
    "ane-ô-nyme",
    "ghost@buster.cdf",
    (SELECT id FROM Droit WHERE signification = "Visiteur")
  );

-- Insertion d'un administrateur (mdp admin)
INSERT INTO Utilisateur(login, password, pseudo, email, droit)
  VALUES
  (
    "admin",
    "$2y$10$rgzp3YNtRDBCMYyaNb7SYOK7e96AaaLqaPlRUKdHhOij.TP0J1.ZG",
    "Admin",
    "admin@root.fr",
    (SELECT id FROM Droit WHERE signification = "Administrateur")
  );



-- Insertion de membres supplémentaires pour les premiers topics/commentaires...

INSERT INTO Utilisateur(login, password, pseudo, email, droit)
  VALUES
  (
    "J-P",
    "$2y$10$/fuh7u6ygdWep/4Yc/csSuxG/kTIRJND3SmYhLMI8CGGMfo.FO.BK",
    "Jean Passe",
    "mpaper@unistra.fr",
    (SELECT id FROM Droit WHERE signification = "Modérateur")
  );

INSERT INTO Utilisateur(login, password, pseudo, email, droit)
  VALUES
  (
    "LF",
    "$2y$10$yEEItAiKavP8pKvRDDagAOkGp9ww.gRZa5dRDxUOs0WtY3VNGZyi.",
    "Lucy Fer",
    "abc@unistra.fr",
    (SELECT id FROM Droit WHERE signification = "Membre")
  );