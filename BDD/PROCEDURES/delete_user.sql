/*
 * Procedure qui supprime un Utilisateur.
 * Parametres:
              i : Id de l'urilisateur à supprimer.
*/

delimiter //

CREATE PROCEDURE delete_user (IN i INT(11))
  BEGIN
    UPDATE Categorie SET createur = 1 WHERE createur = i;
    UPDATE Topic SET createur = 1 WHERE createur = i;
    UPDATE Commentaire SET createur = 1 WHERE createur = i;
    DELETE FROM Commentaire_Note WHERE juge = i;
    
    UPDATE Commentaire c
      SET c.note = COALESCE((SELECT SUM(note) FROM Commentaire_Note cn
                    GROUP BY commentaire HAVING commentaire = c.id), 0);

    DELETE FROM Topic_Note WHERE juge = i;
   
    UPDATE Topic t
      SET t.moyenne = COALESCE((SELECT AVG(note) FROM Topic_Note tn
                       GROUP BY topic HAVING topic = t.id),-1);
        
    DELETE FROM Utilisateur WHERE id = i;
  END//

delimiter ;