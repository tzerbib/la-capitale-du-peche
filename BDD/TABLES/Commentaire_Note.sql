CREATE TABLE IF NOT EXISTS Commentaire_Note
(
    id INT AUTO_INCREMENT,
    note TINYINT NOT NULL,
    juge INT NOT NULL,
    commentaire INT NOT NULL,

    PRIMARY KEY(id),
    UNIQUE(juge, commentaire),
    FOREIGN KEY(juge) REFERENCES Utilisateur(id),
    FOREIGN KEY(commentaire) REFERENCES Commentaire(id)
);