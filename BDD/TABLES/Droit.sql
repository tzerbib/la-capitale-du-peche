/*
 * Tables représentant les rôles des utilisateurs, ainsi que leur poids.
 * Rôles : 
            0 : Visiteur
            1 : Membre
            2 : Modérateur
            3 : Administrateur
*/

CREATE TABLE IF NOT EXISTS Droit
(
  id INT AUTO_INCREMENT,
  hierarchie INT DEFAULT 0,
  signification ENUM("Visiteur", "Membre", "Modérateur", "Administrateur")
                UNIQUE DEFAULT "Visiteur",

  PRIMARY KEY(id)
);