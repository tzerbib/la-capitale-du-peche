<?php
class Controller_Accueil
{
  public function view()
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
      require_once "models/users.php";
      if(!isset($_SESSION['user']))
      {
        try
        {
          $User = new User("Anonyme");
          $_SESSION['user'] = serialize($User);
        }
        catch(Exception $e)
        {
          $_SESSION['msg_co'] =
          "Erreur lors de l'instanciation d'un utilisateur : ";
          $_SESSION['msg_co'] .= $e->getMessage();
          header('Location: '.$_SESSION['root']);
          exit;
        }
      }
      else
      {
        $User = unserialize($_SESSION['user']);
      }
      if(!isset($_SESSION['corps']))
      {
        $_SESSION['corps'] = 'views/corps/viewCorpsAccueil.php';
      }

      // Listing des catégories (pour le nav)
      require_once 'controllers/categories.php';
      $_SESSION['Categories'] = Controller_Categories::liste_categories();

      include_once 'views/viewAccueil.php';
      unset($_SESSION['Categories']);
    }
  }


  public function authenticate()
  {  
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
      if(isset($_POST['login']) && isset($_POST['password'])
      && isset($_SESSION['user']))
      {
        try
        {
          require_once 'models/bdd.php';
          require_once 'models/model_base.php';
          Model_Base::set_db(new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD));
        }
        catch(Exception $e)
        {
          $_SESSION['msg_co'] = "Erreur lors de la connexion à la BDD : ";
          $_SESSION['msg_co'] .= $e->getMessage();
          header('Location: '.$_SESSION['root']);
          exit;
        }
        
        require_once "models/users.php";
        $User = unserialize($_SESSION['user']);
        $User->set_login(htmlentities($_POST['login']));
        $User->set_password(htmlentities($_POST['password']));
        

        switch($User->exists())
        {
          case 0: // SUCCESS
          {
            $User->set_connecte(true);
            $_SESSION['user'] = serialize($User);
            header('Location: '.$_SESSION['root']);
            exit;
          }
          case 1: // Invalid password
          {
            $_SESSION['msg_co'] = "Mot de passe incorrect";
            break;
          }
          case 2: // Invalid login
          {
            $_SESSION['msg_co'] = "Cet utilisateur n'existe pas";
            break;
          }
        }
      }
      else
      {
        $_SESSION['msg_co'] = "Toutes les infos n'ont pas été transmises";
      }
      header('Location: '.$_SESSION['root']);
      exit;
    }
    else
    {
      header('Location: '.$_SESSION['root']);
      exit;
    }
  }

  public function signout()
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
      if(isset($_SESSION['user']))
      {
        require_once "models/users.php";
        $User = new User(htmlentities("Anonyme"));
        $_SESSION['user'] = serialize($User);
        header('Location: '.$_SESSION['root']);
        exit;
      }
    }
    else
    {
      header('Location: '.$_SESSION['root']);
      exit;
    }
  }

  public function aPropos()
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
      // Changement du corps de la page
      $_SESSION['corps'] = 'views/corps/viewCorpsAPropos.php';
      $this->view();
      unset($_SESSION['corps']);
    }
  }

  public function contact()
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
      // Changement du corps de la page
      $_SESSION['corps'] = 'views/corps/viewCorpsContact.php';
      $this->view();
      unset($_SESSION['corps']);
    }
  }
}
