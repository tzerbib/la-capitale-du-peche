/*
 * Procedure qui liste l'id et l'intitule de toutes les categories.
*/

delimiter //

CREATE PROCEDURE liste_users ()
  BEGIN
    SELECT id, pseudo, droit FROM Utilisateur;
  END//

delimiter ;