<?php
$Topic = unserialize($_SESSION['Topic']);
$note_max = 10;
$note = $Topic->get_moyenne()*100/$note_max;
?>

<h1 class="m-3"><?php echo $Topic->get_intitule();?></h1>

<div class="text-left m-4">
  <div class="d-flex flex-columns align-items-center">
    <div id="Notation" class="d-inline-block m-2">
      <span id="typeNote">Moyenne : </span>
      <div class="semi-stars" onmousemove="changer_note(this)"
          onclick="noter(<?php echo $Topic->get_id(); ?>)">
        <div id="full-stars" class="full-stars" 
            style="width: <?php echo $note;?>%;"></div>
      </div>
      <span id="valeurNote"><?php echo $Topic->get_moyenne().'/'.$note_max;?>
      </span>
    </div>
      <div class="alert m-2 my-lg-0 alert-area" role="alert"
           id="msg-alert-topic-note"></div>
  </div>
  <div class="bg-light border border-dark p-3">
    <p><?php echo $Topic->get_contenu();?></p>
  </div>

  <div id="comment_area" class="mt-5 p-2">
    <?php
    foreach ($Topic->get_listeCom() as $Com)
    {
      $com = unserialize($Com);
      $id = $com->get_id();
      $class = "";
      if($com->get_note() < 0)
      {
        $class = "bg-sec text-white";
      }
      else
      {
        $class = "bg-light";
      }
      echo '<div class="d-flex align-items-center w-100">';
        echo '<div class="d-flex flex-column align-items-center ml-2 mr-2
                            align-middle">';
          echo '<button id="plus_'.$id.'" class="arrowMark" 
                        onclick="plussoyer('.$id.')">&#9650;
                </button>';
          echo '<span id="'.$id.'">'.$com->get_note().'</span>';
          echo '<button id="moins_'.$id.'" class="arrowMark"
                        onclick="moinssoyer('.$id.')" >&#9660;
                </button>';
        echo '</div>';
        echo '<div id="com_'.$id.'"
               class="'.$class.' border border-dark m-2 p-3 w-100">';
          echo '<p>'.$com->get_pseudo_createur().' - '.$com->get_date().'</p>';
          echo '<p>'.$com->get_contenu().'</p>';
        echo '</div>';
      echo '</div>';
    }
    ?>
  </div>

  <div class="mt-5">
    <div class="alert alert-danger alert-dismissible fade show alert-area"
         role="alert" id="msg_sending">
    </div>
    <form method="post" class="form mt-2 text-right" id="commenter">
      <label for="newCom" class="sr-only">Votre commentaire</label>
      <textarea id="newCom" name="newCom" class="form-control mb-2"
                value="newCom" placeholder="Votre commentaire"
                required></textarea>
      <button id="sending_button" class="btn btn-success" type="submit">
        <span id="button_span" role="status" aria-hidden="true">
        </span>
        <span id="button_text">Commenter</span>
      </button>
    </form>
    </div>
  </div>
</div>


<script>
// On utilise php ici pour sauvegarder l'adresse de destination du formulaire
  var root = "<?php echo $_SESSION['root']?>";
  var idTopic = <?php echo $Topic->get_id();?>;
  var moyenneTopic = <?php echo $Topic->get_moyenne();?>;
</script>
<script src="<?php echo $_SESSION['root']."public/js/topic.js";?>"></script>