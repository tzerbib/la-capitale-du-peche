/*
 * Procedure qui insère une note dans la table Commentaire_Note.
 * Parametres:
              n : Note attribué au commentaire.
              j : Id de l'utilisateur qui a noté le commentaire.
              c : Id du commentaire noté.
*/

delimiter //

CREATE PROCEDURE noter_commentaire (IN n TINYINT, IN j INT(11), IN c INT(11))
  BEGIN
    INSERT INTO Commentaire_Note(note, juge, commentaire)
      VALUES(n, j, c) ON DUPLICATE KEY UPDATE note = n;
    UPDATE Commentaire SET note = (SELECT COALESCE(SUM(note), 0)
      FROM Commentaire_Note WHERE commentaire = c), date = NOW() WHERE id = c;
    SELECT note FROM Commentaire WHERE id = c;
  END//

delimiter ;