/*
 * Procedure qui insère un commentaire dans la table Commentaire.
 * Parametres:
              c : Contenu du commentaire.
              crea : Id de l'utilisateur qui a écrit le commentaire.
              t : Id du topic commenté.
*/

delimiter //

CREATE PROCEDURE commenter (IN c TEXT, IN crea INT(11), IN t INT(11))
  BEGIN
   INSERT INTO Commentaire(contenu, date, createur, topic)
   VALUES(c, NOW(), crea, t);
   UPDATE Topic SET derniere_modif = NOW() WHERE id = t;
   SELECT LAST_INSERT_ID() as id;
  END//

delimiter ;