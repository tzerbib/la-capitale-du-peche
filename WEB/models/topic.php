<?php

require_once "model_base.php";
require_once "commentaire.php";


class Topic extends Model_Base
{
  protected $_id;
  protected $_intitule;
  protected $_contenu;
  protected $_moyenne;
  protected $_derniere_modif;
  protected $_createur;
  protected $_pseudo_createur;
  protected $_categorie;
  protected $_listeCom;

  public function __construct()
  {
    $ctp = func_num_args(); // nb arguments passés
    $args= func_get_args(); // tableau avec liste arguments
    
    switch($ctp)
    {
      case 4: // id, intitule, contenu, createur, catégorie
      {
        $this->set_categorie($args[3]);
      }
      case 3: // intitule, contenu, createur
      {
        $this->set_createur($args[2]);
      }
      case 2: // intitule, contenu
      {
        $this->set_contenu($args[1]);
      }
      case 1: // intitule
      {
        $this->set_intitule($args[0]);
      }
      case 0:
      {
        break;
      }
      default:
      {
        throw new Exception("Nombre d'arguments du contructeur incorrect.");
      }
    }
  }

  public function set_id($id)
  {
    $this->_id = $id;
  }

  public function get_id()
  {
    return $this->_id;
  }


  public function set_intitule($i)
  {
    $this->_intitule = $i;
  }

  public function get_intitule()
  {
    return $this->_intitule;
  }


  public function set_contenu($c)
  {
    $this->_contenu = $c;
  }

  public function get_contenu()
  {
    return $this->_contenu;
  }


  public function set_moyenne($m)
  {
    $this->_moyenne = $m;
  }

  public function get_moyenne()
  {
    return $this->_moyenne;
  }


  public function set_derniere_modif($m)
  {
    $this->_derniere_modif = $m;
  }

  public function get_derniere_modif()
  {
    return $this->_derniere_modif;
  }


  public function set_createur($c)
  {
    $this->_createur = $c;
  }

  public function get_createur()
  {
    return $this->_createur;
  }

  
  public function set_pseudo_createur($p)
  {
    $this->_pseudo_createur = $p;
  }

  public function get_pseudo_createur()
  {
    return $this->_pseudo_createur;
  }


  public function set_categorie($c)
  {
    $this->_categorie = $c;
  }

  public function get_categorie()
  {
    return $this->_categorie;
  }


  public function set_listeCom($l)
  {
    $this->_listeCom = $l;
  }
  public function get_listeCom()
  {
    return $this->_listeCom;
  }


  public function exists()
  {
    setlocale(LC_TIME, 'fr_FR.utf8', 'fra');
    $statement = self::$_db->prepare('CALL topic_exists(:id)');
    $statement->bindValue(':id', $this->get_id(), PDO::PARAM_INT);
    $statement->execute();

    if($result = $statement->fetch(PDO::FETCH_ASSOC))
    {
      $this->set_intitule($result['intitule']);
      $this->set_contenu($result['contenu']);
      $this->set_moyenne($result['moyenne']);

      $statement->closeCursor();
      $statement = self::$_db->prepare('CALL get_commentaires_topic(:id)');
      $statement->bindValue(':id', $this->get_id(), PDO::PARAM_INT);
      $statement->execute();

      $tabComs = array();
      while($res = $statement->fetch(PDO::FETCH_ASSOC))
      {
        $com = new Commentaire($res['contenu']);
        $com->set_id($res['id']);
        $com->set_note($res['note']);

        // Formattage de la date
        $date = date_create($res['date']);
        $com->set_date(strftime("Posté le %d/%m/%Y à %kh%M", $date->getTimestamp()));
        
        $com->set_pseudo_createur($res['pseudo']);
        $tabComs[] = serialize($com);
      };
      $this->set_listeCom($tabComs);
      $statement->closeCursor();

      return 0; // OK
    }
    return 1; // Id inexistant
  }
}