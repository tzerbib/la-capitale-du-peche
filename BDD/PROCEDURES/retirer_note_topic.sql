/*
 * Procedure qui insère une note dans la table Topic_Note.
 * Parametres:
              j : Id de l'utilisateur qui souhaite retirer sa note.
              c : Id du topic noté.
*/

delimiter //

CREATE PROCEDURE retirer_note_topic (IN j INT(11), IN t INT(11))
  BEGIN
    DELETE FROM Topic_Note WHERE juge = j AND topic = t;
    UPDATE Topic SET moyenne = (SELECT COALESCE(SUM(note), -1)
      FROM Topic_Note WHERE topic = t), derniere_modif = NOW() WHERE id = t;
    SELECT moyenne FROM Topic WHERE id = t;
  END//

delimiter ;