/*
 * Procedure qui renvoie les infos du topic donné en parametre.
 * Parametres: Id = id du topic dont on veut connaître les informations.
*/

delimiter //

CREATE PROCEDURE topic_exists (IN topic_id INT(11))
  BEGIN
    SELECT intitule, contenu, moyenne FROM Topic
    WHERE id = topic_id;
  END//

delimiter ;