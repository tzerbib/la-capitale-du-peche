/*
 * Procedure qui renvoie les infos de l'utilisateur donné en parametre.
 * Parametres: p = pseudo de l'utilisateur.
*/

delimiter //

CREATE PROCEDURE pseudo_exists (IN p VARCHAR(30))
  BEGIN
    SELECT id, password, pseudo, email, droit FROM Utilisateur
    WHERE pseudo = p;
  END//

delimiter ;