<?php
class Controller_Utilisateurs
{
  public function viewAll()
  {
    if($_SERVER['REQUEST_METHOD'] == 'GET')
    {
      try
      {
        require_once 'models/bdd.php';
        require_once 'models/model_base.php';
        require_once 'models/users.php';
        Model_Base::set_db(new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD));
      }
      catch(Exception $e)
      {
        $_SESSION['message'] = "Erreur lors de la connexion à la BDD : ";
        $_SESSION['message'] .= $e->getMessage();
        header('Location: '.$_SESSION['root']);
        exit;
      }

      // Vérification des droits
      if(!isset($_SESSION['user']) 
      || !unserialize($_SESSION['user'])->get_connecte()
      || !unserialize($_SESSION['user'])->hasPermissions("Administrateur"))
      {
        $_SESSION['corps'] = 'views/errors/403.php';
      }
      else
      {
        $_SESSION['corps'] = 'views/corps/viewCorpsListeUtilisateurs.php';
      }

      // Changement du corps de la page
      $_SESSION['Utilisateurs'] = User::getUsers();
      require_once 'controllers/accueil.php';
      $c = new Controller_Accueil;
      $c->view();
      unset($_SESSION['corps']);
    }
  }

  public function delete()
  {
    if($_SERVER['REQUEST_METHOD'] != 'GET')
    {
      header('Location: '.$_SESSION['root']);
    }

    try
    {
      require_once 'models/bdd.php';
      require_once 'models/model_base.php';
      require_once 'models/users.php';
      Model_Base::set_db(new PDO(SQL_DSN, SQL_USERNAME, SQL_PASSWORD));
    }
    catch(Exception $e)
    {
      $_SESSION['message'] = "Erreur lors de la connexion à la BDD : ";
      $_SESSION['message'] .= $e->getMessage();
      header('Location: '.$_SESSION['root']);
      exit;
    }

    // Vérification des droits
    if(!isset($_SESSION['user']) 
    || !unserialize($_SESSION['user'])->get_connecte()
    || !unserialize($_SESSION['user'])->hasPermissions("Administrateur"))
    {
      $_SESSION['corps'] = 'views/errors/403.php';
      require_once 'controllers/accueil.php';
      $c = new Controller_Accueil;
      $c->view();
      unset($_SESSION['corps']);
      exit;
    }

    // Cas sans parametre de la méthode (id utilisateur)
    if(func_num_args() != 1)
    {
      $_SESSION['message'] = "Aucun utilisateur n'a été spécifié";
      header('Location: '.$_SESSION['root'].'index.php/utilisateurs|viewAll');
      exit;
    }
    
    $args = func_get_args();

    $u = new User();
    $u->set_id((int)(htmlentities($args[0])));
    if($u->delete() == 1)
    {
      $_SESSION['message'] = "Erreur lors de la suppression";
    }
    header('Location: '.$_SESSION['root'].'index.php/utilisateurs|viewAll');
  }
}