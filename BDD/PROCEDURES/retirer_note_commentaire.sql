/*
 * Procedure qui insère une note dans la table Commentaire_Note.
 * Parametres:
              j : Id de l'utilisateur qui souhaite retirer sa note.
              c : Id du commentaire noté.
*/

delimiter //

CREATE PROCEDURE retirer_note_commentaire (IN j INT(11), IN c INT(11))
  BEGIN
    DELETE FROM Commentaire_Note WHERE juge = j AND commentaire = c;
    UPDATE Commentaire SET note = (SELECT COALESCE(SUM(note), 0)
      FROM Commentaire_Note WHERE commentaire = c) , date = NOW() WHERE id = c;
    SELECT note FROM Commentaire WHERE id = c;
  END//

delimiter ;