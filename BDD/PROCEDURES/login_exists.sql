/*
 * Procedure qui renvoie les infos de l'utilisateur donné en parametre.
 * Parametres: l = login de l'utilisateur.
*/

delimiter //

CREATE PROCEDURE login_exists (IN l VARCHAR(63))
  BEGIN
    SELECT id, password, pseudo, email, droit FROM Utilisateur
    WHERE login = l;
  END//

delimiter ;