/*
 * Procedure qui insère une note dans la table Topic_Note.
 * Parametres:
              n : Note attribué au topic.
              j : Id de l'utilisateur qui a noté le topic.
              t : Id du topic noté.
*/

delimiter //

CREATE PROCEDURE noter_topic (IN n FLOAT, IN j INT(11), IN t INT(11))
  BEGIN
    INSERT INTO Topic_Note(note, juge, topic)
      VALUES(n, j, t) ON DUPLICATE KEY UPDATE note = n;
    UPDATE Topic SET moyenne = (SELECT COALESCE(AVG(note), -1)
      FROM Topic_Note WHERE topic = t), derniere_modif = NOW() WHERE id = t;
    SELECT moyenne FROM Topic WHERE id = t;
  END//

delimiter ;