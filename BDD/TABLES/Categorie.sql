CREATE TABLE IF NOT EXISTS Categorie
(
    id INT AUTO_INCREMENT,
    intitule VARCHAR(255) UNIQUE NOT NULL,
    createur INT,
    dateCreation DATE,

    PRIMARY KEY(id),
    FOREIGN KEY(createur) REFERENCES Utilisateur(id)
);