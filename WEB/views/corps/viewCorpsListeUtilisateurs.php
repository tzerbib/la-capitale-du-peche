<h1 class="m-3">Liste des utilisateurs</h1>

<div class="mt-5">
  <?php
  if(isset($_SESSION['Utilisateurs']))
  {
    echo '<ul class="list-group">';
      foreach($_SESSION['Utilisateurs'] as $value)
      {
        $User = unserialize($value);

        echo '<li class="list-group-item d-flex justify-content-between
                         align-items-center">';
          echo $User->get_pseudo().", ".$User->get_signification();
          echo '<button class="badge badge-danger badge-pill"
                   id="'.$User->get_id().'"
                   onclick="delete_user('.$User->get_id().')">
                  &#10007;
                </button>';
        echo '</li>';
      }
    echo '</ul>';
  }
  ?>
</div>

<script>

function delete_user(id)
{
  if(confirm("Confirmer la suppression"))
  {
    window.location.href = "<?php echo $_SESSION['root']; ?>index.php/utilisateurs|delete|"+id;
  }
}

</script>