/*
 * Procedure qui liste l'id et l'intitule de toutes les categories.
*/

delimiter //

CREATE PROCEDURE liste_categories ()
  BEGIN
    SELECT id, intitule FROM Categorie ORDER BY dateCreation DESC;
  END//

delimiter ;