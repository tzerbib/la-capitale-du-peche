/*
 * Procedure qui renvoie les infos de l'utilisateur donné en parametre.
 * Parametres: e = email de l'utilisateur.
*/

delimiter //

CREATE PROCEDURE email_exists (IN e VARCHAR(255))
  BEGIN
    SELECT id, password, pseudo, email, droit FROM Utilisateur
    WHERE email = e;
  END//

delimiter ;