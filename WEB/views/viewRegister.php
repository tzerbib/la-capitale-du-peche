<!doctype html>
<html lang="fr">
  <head>
    <title>Inscription - La Capitale du Péché</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,
                                   shrink-to-fit=no">
    <meta name="description" content="Forum sur les péchés capitaux"/>
    <meta name="keyword"
          content="Forum, péchés, discussion, inscription, prog web"/>
    <meta name="author" content="Timothée ZERBIB"/>
    <link rel="icon" type="image/png"
          href="<?php echo $_SESSION['root'];?>public/images/favicon.png">
    <link rel="shortcut icon" type="image/x-icon"
          href="public/images/favicon.ico">
    <link rel="stylesheet" 
          href="<?php echo $_SESSION['root'];?>public/css/base.css">
    <link rel="stylesheet" 
          href="<?php echo $_SESSION['root'];?>public/css/register.css">
    <link rel="stylesheet"
          href=
  "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity=
  "sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
  </head>


  <body style="background-color: antiquewhite;">
    <form method="post"
          class="form-signin mx-auto p-4 m-3 border border-primary"
          onSubmit="return checkPassword(this)"
          action=
          <?php echo $_SESSION['root']."index.php/register|register";?>>
      <div class="text-center mb-4">
        <a href=<?php echo $_SESSION['root']; ?>>
          <img class="mb-2" alt="Logo du site" width="80" height="80"
              src="<?php echo $_SESSION['root'];?>public/images/favicon.png">
        </a>
        <h1 class="h3 mb-3 font-weight-normal">S'inscrire</h1>
      </div>

      <?php
      if(isset($_SESSION['message']))
      {
        echo '<div class="alert alert-danger mr-sm-2 my-lg-0">';
          echo $_SESSION['message'];
          unset($_SESSION['message']);
        echo '</div>';
      }
      ?>

      <div class="form-label-group mb-2">
        <label for="login" class="sr-only">Login</label>
        <input type="input" id="login" name="login" class="form-control"
               data-toggle="tooltip" data-placement="right"
               title="Le login est utilisé lors de la connexion"
               placeholder="Login" required autofocus>
      </div>

      <div class="form-label-group mb-2">
        <label for="pseudo" class="sr-only">Pseudo</label>
        <input type="input" id="pseudo" name="pseudo" class="form-control"
               data-toggle="tooltip" data-placement="right"
               title="Le pseudo est visible des autres utilisateurs"
               placeholder="Pseudo" required>
      </div>

      <div class="form-label-group mb-2">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="inputPassword"
               class="form-control" placeholder="Password" required>
      </div>

      <div class="form-label-group mb-2">
        <label for="confirmPassword" class="sr-only">Confirm password</label>
        <input type="password" id="confirmPassword" name="confirmPassword"
               class="form-control" placeholder="Confirm password" required>
      </div>

      <div class="form-label-group mb-2">
        <label for="email" class="sr-only">Email</label>
        <input type="email" id="email" name="email" class="form-control"
               placeholder="Email" required>
      </div>


      <button class="btn btn-lg btn-primary btn-block mt-4 mb-4"
              type="submit">S'inscrire</button>

      <p class="mb-0">
        Déjà membre ? <a href="<?php echo $_SESSION['root'];?>">Connecte toi !</a>
      </p>
    </form>
  </body>
</html>