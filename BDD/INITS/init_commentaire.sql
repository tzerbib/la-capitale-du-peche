/*
 * Fichier d'initialisation de la table Commentaire
 * 
 * Non nécessaire au bon fonctionnement du site.
 * Ces insertions permettent simplement d'avoir un site non vide au départ...
*/


INSERT INTO Commentaire(contenu, date, createur, topic)
VALUES
(
  "Franchement, un des meilleur topic de test que j'ai vu de ma vie.",
  NOW(),
  (SELECT id FROM Utilisateur WHERE pseudo = "Admin"),
  (SELECT id FROM Topic WHERE intitule = "Selon Wikipédia"
   AND categorie = (SELECT id FROM Categorie WHERE intitule = "La luxure"))
);

INSERT INTO Commentaire(contenu, date, createur, topic)
VALUES
(
  "Farpaitement d'accord.",
  NOW(),
  (SELECT id FROM Utilisateur WHERE pseudo = "Jean Passe"),
  (SELECT id FROM Topic WHERE intitule = "Selon Wikipédia"
   AND categorie = (SELECT id FROM Categorie WHERE intitule = "La luxure"))
);
